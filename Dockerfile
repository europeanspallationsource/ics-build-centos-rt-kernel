FROM centos:7.4.1708

RUN yum update -y \
  && yum groups mark convert \
  && yum groups install -y "Development Tools" \
  && yum install -y \
  ncurses-devel \
  hmaccalc \
  zlib-devel \
  binutils-devel \
  elfutils-libelf-devel \
  net-tools \
  bc \
  pesign \
  xmlto \
  asciidoc \
  which \
  openssl \
  desktop-file-utils \
  python-devel \
  newt-devel \
  perl-ExtUtils-Embed \
  tree

RUN mkdir -p /build

WORKDIR /build

COPY ./build.sh /build/
COPY ./patches /tmp/patches

CMD ["/build/build.sh"]
