#!/bin/bash

# Define all tags
KERNEL_RT_TAG="imports/c7-rt/kernel-rt-3.10.0-693.17.1.rt56.636.el7"
RT_SETUP_TAG="imports/c7-rt/rt-setup-1.59-5.el7"
RTCTL_TAG="imports/c7-rt/rtctl-1.13-2.el7"
TUNED_TAG="imports/c7/tuned-2.8.0-5.el7_4.2"

# To update if new patches are applied
BUILDID=".ESS1"

cd /build
git clone https://git.centos.org/git/centos-git-common.git
git clone https://git.centos.org/git/rpms/kernel-rt
git clone https://git.centos.org/git/rpms/rt-setup
git clone https://git.centos.org/git/rpms/rtctl
git clone https://git.centos.org/git/rpms/tuned

cd /build/kernel-rt
git checkout ${KERNEL_RT_TAG} && ../centos-git-common/get_sources.sh -b c7-rt

nb_patches=$(ls /tmp/patches | wc -l)
if [[ "$nb_patches" != "0" ]]
then
  echo "Copy patches and modify kernel-rt.spec to apply them"
  cp /tmp/patches/*.patch SOURCES/
  cp -p SPECS/kernel-rt.spec SPECS/kernel-rt.spec.org
  for patch in $(ls /tmp/patches/)
  do
    sed -i "/## Apply Patches here/a ApplyPatch ${patch}" SPECS/kernel-rt.spec
  done
  sed -i "/%define buildid/a %define buildid ${BUILDID}" SPECS/kernel-rt.spec

  ## Temporary patch for imports/c7-rt/kernel-rt-3.10.0-514.26.1.rt56.442.el7
  ## pkg_release includes buildid
  ## pkg_release_simple doesn't include el7
  #sed -i 's/Source0: %{name}-%{rpmversion}-%{pkg_release}.tar.xz/Source0: kernel-rt-3.10.0-514.26.1.rt56.442.el7.tar.xz/' SPECS/kernel-rt.spec

  diff -u SPECS/kernel-rt.spec.org SPECS/kernel-rt.spec
else
  echo "No patch to apply"
fi

rpmbuild --define "%_topdir $(pwd)" --target $(uname -m) --without debug --without trace -ba SPECS/kernel-rt.spec
rpmbuild --define "%_topdir $(pwd)" --target noarch --with firmware --without doc -ba SPECS/kernel-rt.spec
tree RPMS
rm -f RPMS/x86_64/kernel-rt-debuginfo* RPMS/x86_64/kernel-rt-kvm*
cp RPMS/x86_64/*.rpm /RPMS
cp RPMS/noarch/kernel-rt-firmware-*.rpm /RPMS

cd /build/rt-setup
git checkout ${RT_SETUP_TAG} && ../centos-git-common/get_sources.sh -b c7-rt
rpmbuild --define "%_topdir $(pwd)" --target noarch -ba SPECS/rt-setup.spec
tree RPMS
cp RPMS/noarch/rt-setup*.rpm /RPMS

cd /build/rtctl
git checkout ${RTCTL_TAG} && ../centos-git-common/get_sources.sh -b c7-rt
rpmbuild --define "%_topdir `pwd`" --target noarch -ba SPECS/rtctl.spec
tree RPMS
cp RPMS/noarch/rtctl*.rpm /RPMS

cd /build/tuned
git checkout ${TUNED_TAG} && ../centos-git-common/get_sources.sh -b c7
rpmbuild --define "%_topdir `pwd`" --target noarch -ba SPECS/tuned.spec
tree RPMS
cp RPMS/noarch/tuned-profiles-realtime*.rpm /RPMS
