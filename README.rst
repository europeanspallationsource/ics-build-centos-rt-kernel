ics-build-centos-rt-kernel
==========================

**DEPRECATED**

This repository was moved to GitLab and splitted in 4:

- https://gitlab.esss.lu.se/ics-rpms/kernel-rt
- https://gitlab.esss.lu.se/ics-rpms/rtctl
- https://gitlab.esss.lu.se/ics-rpms/rt-setup
- https://gitlab.esss.lu.se/ics-rpms/tuned

Repository to build CentOS 7 RT kernel.
The kernel is built inside a docker image.

Usage
-----

::

    $ docker build -t rt-kernel .
    $ docker run --rm -v $(pwd)/RPMS:/RPMS rt-kernel


This will build the latest CentOS 7 RT kernel and put the resulting RPMs
under RPMS in the current directory.
